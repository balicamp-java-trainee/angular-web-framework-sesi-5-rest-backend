import { AuthGuardService } from './helper/auth-guard.service';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductDetailComponent } from './product/product-detail/product-detail.component';
import { CreateProductComponent } from './product/create-product/create-product.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './login/login.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'product',
    component: ProductListComponent,
    data: { title: 'Data Product' },
    canActivate: [AuthGuardService]
  },
  {
    path: 'product-detail/:id',
    component: ProductDetailComponent,
    data: { title: 'Data Detail' },
    canActivate: [AuthGuardService]
  },
  {
    path: 'product-create',
    component: CreateProductComponent,
    data: { title: 'Add New Product' },
    canActivate: [AuthGuardService]
  },
  {
    path: 'product-edit/:id',
    component: CreateProductComponent,
    data: { title: 'Edit Product' },
    canActivate: [AuthGuardService]
  },
  {
    path: 'upload-file',
    component: UploadFileComponent,
    data: { title: 'Upload File' },
    canActivate: [AuthGuardService]
  },
  {
    path: 'home',
    component: LoginComponent
  },
  {
    path: '**',
    component : NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
