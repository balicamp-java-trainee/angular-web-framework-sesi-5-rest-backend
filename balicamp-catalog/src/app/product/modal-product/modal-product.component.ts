import { ProductService } from './../service/product.service';
import { FormGroup, NgForm, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Product } from './../model/product.model';
import { Component, OnInit, Inject, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PageActionAccessor } from 'src/app/helper/page-action.accessor';

@Component({
  selector: 'app-modal-product',
  templateUrl: './modal-product.component.html',
  styleUrls: ['./modal-product.component.css']
})
export class ModalProductComponent implements OnInit {

  productForm: FormGroup;
  id: string = '';
  code: string = '';
  name: string = '';
  weight: number = 1;
  price: number = 0;

  @ViewChild('priceInputRef') priceInputRef: ElementRef;

  constructor(
    public dialogRef: MatDialogRef<ModalProductComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Product,
    private productService: ProductService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.productForm = this.formBuilder.group({
      'id': [null],
      'code': [null, Validators.required],
      'name': [null, Validators.required],
      'weight': [1],
      'price': [0]
    });
    this.fetchData();
  }

  fetchData() {
    this.productForm.setValue({
      id: this.data.id,
      code: this.data.code,
      name: this.data.name,
      weight: this.data.weight,
      price: this.data.price
    });
  }

  onFormSubmit(form: NgForm) {
    this.productService.updateProduct(form)
      .subscribe(res => {
        this.dialogRef.close();
      }, (err) => {
        console.error(err);
      });
  }

  hide() {
    this.dialogRef.close();
  }
}
