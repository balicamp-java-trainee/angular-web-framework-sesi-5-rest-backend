import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../service/product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  product = {};

  constructor(private productService: ProductService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.fetchData();
  }

  fetchData(){
    this.productService.getProductById(this.route.snapshot.params['id'])
    .subscribe(data => {
      this.product = data;
    },
    err => {
      console.error(err)
    });
  }

  deleteProduct(id){
    this.productService.deleteProduct(id).subscribe(res => {
      if(res.status == 'success'){
        this.router.navigate(['/product']);
      }
    },
    err => {
      console.error(err)
    })
  }
}
