@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CorsFilter implements Filter {

  private final Logger LOGGER = LoggerFactory.getLogger(CorsFilter.class);

  public CorsFilter() {
    LOGGER.info("Simple CorsFilter init");
  }

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
  }

  @Override
  public void doFilter(ServletRequest servletRequest,
  ServletResponse servletResponse, FilterChain filterChain)
  throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) servletRequest;
    HttpServletResponse response = (HttpServletResponse) servletResponse;

    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET,
    OPTIONS, DELETE");
    response.setHeader("Access-Control-Allow-Headers", "x-requested-with,
    Authorization, Content-Type, enctype, x-forwarded-for, responseType");
    response.setHeader("Access-Control-Expose-Headers", "x-requested-with,
    Authorization, Content-Type, enctype, x-forwarded-for, responseType");
    response.setHeader("Access-Control-Max-Age", "3600");
    if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
        response.setStatus(HttpServletResponse.SC_OK);
    } else {
        filterChain.doFilter(servletRequest, response);
    }
  }

  @Override
  public void destroy() {
  }
}
