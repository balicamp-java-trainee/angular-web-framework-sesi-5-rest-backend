import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import { UploadFileService } from './service/upload-file.service';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.css']
})
export class UploadFileComponent implements OnInit {

  fileName = [];
  disableBtn: boolean = false;
  isValidating: boolean = true;
  status: boolean = true;
  complete: boolean = false;
  currentFileUpload: File;
  selectedFiles: FileList;
  formData = new FormData();
  progress: { percentage: number } = { percentage: 0 }

  @ViewChild('fileInput') fileInput;

  constructor(private uploadFileService: UploadFileService) { }

  ngOnInit() {
  }

  selectFile(event) {
    this.fileName = []
    this.selectedFiles = this.fileInput.nativeElement.files;
    for (let i = 0; i < this.selectedFiles.length; i++) {
      this.fileName.push(this.selectedFiles.item(i).name);
    }
    this.status = true;
    this.complete = false;
    this.disableBtn = true;
  }

  doUpload() {
    this.selectedFiles = this.fileInput.nativeElement.files;
    let fileCount: number = this.selectedFiles.length;

    if (fileCount > 0) {
      for (let i = 0; i < fileCount; i++) {
        //this.isValidating = true;
        this.isValidating = false;
        this.formData.append(this.selectedFiles.item(i).name,
        this.selectedFiles.item(i));
        this.currentFileUpload = this.selectedFiles.item(i);
        this.doPush()
      }
    } else {
      return;
    }
  }

  doPush() {
    if (!this.isValidating) {
      this.progress.percentage = 0;
      this.uploadFileService.pushFileToStorage(this.formData)
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress.percentage = Math.round(100 * event.loaded / event.total);
        } else
          if (event instanceof HttpResponse) {
            if (event.status === 200) {
              this.doResetUpload();
              this.status = true;
              this.complete = true;
            }
          }
      }, err => {
        this.doResetUpload();
        this.status = false;
        this.complete = false;
      });
    }
  }

  doResetUpload() {
    this.formData = new FormData();
    this.fileInput.nativeElement.value = "";
    this.selectedFiles = this.fileInput.nativeElement.files;
    this.currentFileUpload = this.selectedFiles.item(0);
    this.disableBtn = false;
    this.fileName = [];
    this.progress.percentage = 0;
    this.selectedFiles = undefined;
    this.isValidating = true;
  }

  doReset() {
    this.doResetUpload();
  }
}
