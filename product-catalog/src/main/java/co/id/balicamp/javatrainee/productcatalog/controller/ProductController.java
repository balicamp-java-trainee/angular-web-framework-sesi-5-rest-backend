package co.id.balicamp.javatrainee.productcatalog.controller;

import co.id.balicamp.javatrainee.productcatalog.model.BaseResponseDto;
import co.id.balicamp.javatrainee.productcatalog.model.ProductDto;
import co.id.balicamp.javatrainee.productcatalog.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-03-08
 * Time: 18:32
 */
@Controller
public class ProductController {
    @Autowired
    private IProductService productService;

    @RequestMapping("/")
    public String hello(){
        return "Hello Spring Boot";
    }

    @RequestMapping(value = "/view-products", method = RequestMethod.POST)
    public ResponseEntity<List<ProductDto>> getProducts(Pageable page)  {
        return new ResponseEntity<List<ProductDto>>(productService.getAllProducts(page), HttpStatus.OK);
    }

    @RequestMapping(value = "/view-products/{idProduct}", method = RequestMethod.POST)
    public ResponseEntity<ProductDto> getProductById(@PathVariable("idProduct") String id) {
        return new ResponseEntity<ProductDto>(productService.getProductById(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/transaction-add-product", method = RequestMethod.POST)
    public ResponseEntity<BaseResponseDto> addProduct(@RequestBody ProductDto product) {
        return new ResponseEntity<BaseResponseDto>(productService.addDataProduct(product), HttpStatus.OK);
    }

    @RequestMapping(value = "/transaction-update-product", method = RequestMethod.POST)
    public ResponseEntity<BaseResponseDto> updateProduct(@RequestBody ProductDto product){
        return new ResponseEntity<BaseResponseDto>(productService.updateDataProduct(product), HttpStatus.OK);
    }

    @RequestMapping(value = "/transaction-delete-product/{id}", method = RequestMethod.POST)
    public ResponseEntity<BaseResponseDto> deleteProduct(@PathVariable String id){
        return new ResponseEntity<BaseResponseDto>(productService.deleteProductById(id), HttpStatus.OK);
    }
}
