package co.id.balicamp.javatrainee.productcatalog.util;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-03-14
 * Time: 10:47
 */
public enum FileSizeUnit {

    bytes("bytes"),
    kilobytes("kilobytes"),
    megabytes("megabytes"),
    gigabytes("gigabytes"),
    terabytes("terabytes"),
    petabytes("petabytes"),
    exabytes("exabytes"),
    zettabytes("zettabytes"),
    yottabytes("yottabytes");

    private String sizeUnit;

    FileSizeUnit(String sizeUnit) {
        this.sizeUnit = sizeUnit;
    }

    public String getSizeUnit() {
        return sizeUnit;
    }
}
