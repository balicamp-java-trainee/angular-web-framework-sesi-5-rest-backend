package co.id.balicamp.javatrainee.productcatalog.service.impl;

import co.id.balicamp.javatrainee.productcatalog.model.FileMetadataDto;
import co.id.balicamp.javatrainee.productcatalog.service.IFileService;
import co.id.balicamp.javatrainee.productcatalog.util.FileMetaUtils;
import co.id.balicamp.javatrainee.productcatalog.util.FileSizeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.*;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-03-14
 * Time: 10:26
 */
@Service
public class FileServiceImpl implements IFileService {

    @Value("${fileMetadata.base.dir:/Users/GusdeGita/Documents/files}")
    protected String BASE_DIR_FILE;

    @Autowired
    private FileMetaUtils fileUtils;

    @Override
    public List<FileMetadataDto> doUpload(MultipartHttpServletRequest multipartRequest) {
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        List<FileMetadataDto> fileMetadatas = new ArrayList<>();

        fileMap.values().forEach(file -> {
            String fileName = file.getOriginalFilename();
            try {
                byte[] bytes = file.getBytes();
                String fileChecksum = fileUtils.writeToFile(BASE_DIR_FILE, bytes);

                FileMetadataDto fileDto = new FileMetadataDto(new Date(),
                        fileUtils.getFilenameWithoutExtension(fileName),
                        fileName,
                        fileUtils.getExtension(fileName),
                        BASE_DIR_FILE,
                        fileChecksum,
                        fileUtils.getSize(file, FileSizeUnit.bytes),
                        fileUtils.getFilePathString(fileChecksum, BASE_DIR_FILE, false),
                        fileUtils.getContentTypeFromSource(fileName));

                fileMetadatas.add(fileDto);

            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        return fileMetadatas;
    }
}
