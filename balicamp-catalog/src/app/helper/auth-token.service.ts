import { SessionCheckerService } from './session-checker.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthConstantModel } from './auth.constant.model';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthTokenService {

  private authConstant: AuthConstantModel = new AuthConstantModel();
  private headers = {
    headers: new HttpHeaders({
      "Content-Type": "application/x-www-form-urlencoded",
      "Authorization": "Basic " + btoa(this.authConstant.client_id + ":" + this.authConstant.client_secret),
      "Accept": "*/*"
    })
  };

  constructor(
    private http: HttpClient, 
    private router: Router, 
    private sessionChecker: SessionCheckerService
  ) { }

  login(username, password) {
    const body = new URLSearchParams();
    body.append("client_id", this.authConstant.client_id);
    //body.append("client_secret", this.authConstant.client_secret);
    body.append("grant_type", this.authConstant.grant_type);
    body.append("username", username);
    body.append("password", password);
    // const headers = new HttpHeaders({
    //   "Content-Type": "application/x-www-form-urlencoded",
    //   "Authorization": "Basic " + btoa(this.authConstant.client_id + ":" + this.authConstant.client_secret),
    //   "Accept": "*/*"
    // })
    //console.log(body.toString(), ' - headers : ', headers)

    this.http.post(this.authConstant.url, body.toString(), this.headers)
      .subscribe((res: Response) => {
        //console.log('response_token ', res)
        localStorage.setItem("access_token", res["access_token"]);
        this.sessionChecker.setIsSessionValid(true);
        this.router.navigate(['/product']);
      })
  }

  logout() {
    localStorage.removeItem("access_token");
  }

  isLogin() {
    return localStorage.getItem("access_token") ? true : false;
  }

  getAccessToken(): string {
    return (this.isLogin) ? localStorage.getItem("access_token") : null;
  }
}
