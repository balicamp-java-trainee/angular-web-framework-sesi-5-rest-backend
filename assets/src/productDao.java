package co.id.balicamp.javatrainee.productcatalog.repository;

import co.id.balicamp.javatrainee.productcatalog.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductDao
  extends JpaRepository<ProductEntity, String> {
}
