import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpRequest, HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {

  private productApiUrl = "http://localhost:8080/transaction/upload-file";
  private httpOptions = {
    reportProgress: true,
    headers: new HttpHeaders({
      'enctype': 'multipart/form-data'
    })
  };

  constructor(private https: HttpClient) { }

  pushFileToStorage(file: any): Observable<HttpEvent<{}>> {
    const req = new HttpRequest('POST', this.productApiUrl, file, this.httpOptions);
    return this.https.request(req);
  }
}
