import { PageActionAccessor } from 'src/app/helper/page-action.accessor';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { CONFIG } from 'src/app/helper/app.constant';
import { AuthGuardService } from 'src/app/helper/auth-guard.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends PageActionAccessor {

  //private productApiUrl = "http://localhost:8080";
  private token = `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsibXMvYWRtaW4iLCJtcy91c2VyIiwibXcvYWRtaW5hcHAiXSwidXNlcl9uYW1lIjoiYWRtaW4iLCJzY29wZSI6WyJyb2xlX2FkbWluIl0sImV4cCI6MTU1MjgwMDk1MSwiYXV0aG9yaXRpZXMiOlsicm9sZV9hZG1pbiIsImNhbl91cGRhdGVfdXNlciIsImNhbl9yZWFkX3VzZXIiLCJjYW5fY3JlYXRlX3VzZXIiLCJjYW5fZGVsZXRlX3VzZXIiXSwianRpIjoiMmE1OWYwOTQtNjcxNS00ZDMyLTk1OGItMjFiZjc2NjI3M2MyIiwiZW1haWwiOiJhZG1pbkBleGFtcGxlLmNvbSIsImNsaWVudF9pZCI6ImFkbWluYXBwIn0.IMDF3MpFdCK3YyaGsVMr51-mmweN04kXSfk17CT-yVk`;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsibXMvYWRtaW4iLCJtcy91c2VyIiwibXcvYWRtaW5hcHAiXSwidXNlcl9uYW1lIjoiYWRtaW4iLCJzY29wZSI6WyJyb2xlX2FkbWluIl0sImV4cCI6MTU1MjgwMTkxNSwiYXV0aG9yaXRpZXMiOlsicm9sZV9hZG1pbiIsImNhbl91cGRhdGVfdXNlciIsImNhbl9yZWFkX3VzZXIiLCJjYW5fY3JlYXRlX3VzZXIiLCJjYW5fZGVsZXRlX3VzZXIiXSwianRpIjoiNjY4ZjFhNmEtZjEyOS00ZDllLTg4MmEtNDE1NDI3ZWYwYTk2IiwiZW1haWwiOiJhZG1pbkBleGFtcGxlLmNvbSIsImNsaWVudF9pZCI6ImFkbWluYXBwIn0.jeKwEUdiU29vM7YFyCvWyRN7ZRQAgLyB3WaiWJ5hpAQ'
    })
  };

  constructor(private http: HttpClient) {
    super();
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      // if(error.status == 401){
      //   this.changeSessionActive(false);
      // }
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(`Something bad happened; please try again later with error code, ${error.status}`);
  };

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  getProducts(): Observable<any> {
    const uri = `${CONFIG.dev.baseUrl}/view-products`;
    return this.http.post(uri, this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  getProductById(id: string): Observable<any> {
    const uri = `${CONFIG.dev.baseUrl}/view-products/${id}`;
    return this.http.post(uri, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  insertProduct(data): Observable<any> {
    const uri = `${CONFIG.dev.baseUrl}/transaction-add-product`;
    return this.http.post(uri, data, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  updateProduct(data): Observable<any> {
    const url = `${CONFIG.dev.baseUrl}/transaction-update-product`;
    return this.http.post(url, data, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  deleteProduct(id: string): Observable<any> {
    const url = `${CONFIG.dev.baseUrl}/transaction-delete-product/${id}`;
    return this.http.post(url, { responseType: 'text' }).pipe(
      catchError(this.handleError)
    );
  }
}