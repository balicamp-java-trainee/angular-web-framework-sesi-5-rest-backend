package co.id.balicamp.javatrainee.productcatalog.service;

import co.id.balicamp.javatrainee.productcatalog.model.FileMetadataDto;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.List;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-03-14
 * Time: 10:24
 */
public interface IFileService {

    List<FileMetadataDto> doUpload(MultipartHttpServletRequest multipartRequest);
}
