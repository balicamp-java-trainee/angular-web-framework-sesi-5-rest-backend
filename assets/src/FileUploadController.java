package co.id.balicamp.javatrainee.productcatalog.controller;

import co.id.balicamp.javatrainee.productcatalog.model.BaseResponseDto;
import co.id.balicamp.javatrainee.productcatalog.model.FileMetadataDto;
import co.id.balicamp.javatrainee.productcatalog.service.IFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.List;

@CrossOrigin(value = "http://localhost:4200", maxAge = 3600)
@RestController
public class FileUploadController {

  @Autowired
  private IFileService fileService;

  @RequestMapping(value = "/transaction/upload-file", method = RequestMethod.POST)
  public ResponseEntity<List<FileMetadataDto>> doUploadFiles(
    MultipartHttpServletRequest multipartRequest)  {
      if (multipartRequest.getFileMap().isEmpty()){
        Map<String, String> responseDto = new HashMap<>();
          responseDto.put("status", "error");
          responseDto.put("message",
              "Uploaded file is empty or the chosen file has no content, " +
                      "please select a file!");
          return new ResponseEntity(responseDto, HttpStatus.OK);
      }else{
          HttpHeaders headers = new HttpHeaders();
          headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
          headers.add(HttpHeaders.PRAGMA, "no-cache");
          headers.add(HttpHeaders.EXPIRES, "0");
          return new ResponseEntity<List<FileMetadataDto>>(
            fileService.doUpload(multipartRequest), headers, HttpStatus.OK);
      }
  }
}
