package co.id.balicamp.javatrainee.productcatalog.repository;

import co.id.balicamp.javatrainee.productcatalog.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-03-08
 * Time: 19:43
 */
public interface ProductDao extends JpaRepository<ProductEntity, String> {
}
