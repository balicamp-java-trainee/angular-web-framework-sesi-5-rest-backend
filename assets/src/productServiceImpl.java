/* Before */
@Override
public List<Product> getAllProducts() {
    return productRepo.getAll();
}

/* After */
protected Log LOGGER = LogFactory.getLog(this.getClass());

@Override
public List<ProductDto> getAllProducts(Pageable page){
  try {
      Page<ProductEntity> productEntities = productDao.findAll(page);
      List<ProductDto> productDtos = new ArrayList<>();
      if(productEntities != null) {
          productDtos = ObjectMapperUtils.mapAll(
            productEntities.getContent(), ProductDto.class);
      }
      return productDtos;
  }catch (Exception e) {
      LOGGER.error("error while get all data products ", e);
      return null;
  }
}
