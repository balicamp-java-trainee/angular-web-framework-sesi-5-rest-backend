import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, NgForm, FormControl } from '@angular/forms';
import { ProductService } from '../service/product.service';
import AutoNumeric from 'autonumeric';
import { PageActionAccessor } from 'src/app/helper/page-action.accessor';
import { AuthTokenService } from 'src/app/helper/auth-token.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  productForm: FormGroup;
  editMode = false;
  id: string;
  status: boolean = true;

  code: string = '';
  name: string = '';
  weight: number = 0;
  price: number = 0;

  @ViewChild('priceInputRef') priceInputRef: ElementRef;

  //? Custom Numeric Library properties
  currencyMaskOptions: any =
    {
      showOnlyNumbersOnFocus: true,
      decimalPlaces: 0,
      emptyInputBehavior: "null",
      maximumValue: "9999999999999999"
    }
    
  constructor(private productService: ProductService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  loadedFeature = 'product';

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }

  ngOnInit() {
    //? init for numeric 
    new AutoNumeric(this.priceInputRef.nativeElement, this.currencyMaskOptions);

    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.editMode = params['id'] != null;
    });
    this.initForm();
  }

  initForm() {
    if (this.editMode) {
      this.productService.getProductById(this.id).subscribe(res => {
        if (res) {
          this.productForm.get('id').setValue(this.id);
          this.productForm.get('code').setValue(res.code);
          this.productForm.get('name').setValue(res.name);
          this.productForm.get('weight').setValue(res.weight);
          this.productForm.get('price').setValue(res.price);
        }
      }, err => {
        console.error(err)
      });
    }

    this.productForm = new FormGroup({
      'id': new FormControl(this.id),
      'code': new FormControl(this.code, Validators.required),
      'name': new FormControl(this.name, Validators.required),
      'weight': new FormControl(this.weight),
      'price': new FormControl(this.price)
    });
  }

  onSubmit() {
    // console.log('ambil pakai cara 1 ', this.productForm.get('price').value)
    // console.log('ambil pakai cara 2 ',this.productForm.value.price)
    // console.log(this.productForm)
    this.productForm.get('price').setValue(this.doUnformatedValue(this.productForm.value.price));
    if (this.editMode) {
      this.productService.updateProduct(this.productForm.value)
        .subscribe(res => {
          this.doVerifyResult(res);
        }, err => {
          //this.setSessionActive(err);
          console.error(err)
        });
    } else {
      this.productService.insertProduct(this.productForm.value)
        .subscribe(res => {
          this.doVerifyResult(res);
        }, err => {
          //this.setSessionActive(err);
          console.error(err)
        });
    }
  }

  doVerifyResult(res) {
    if (res.status == 'success') {
      this.status = true;
      this.goToList();
    } else {
      this.status = false;
    }
  }

  goToList() {
    this.router.navigate(['/product']);
  }

  doUnformatedValue(val) {
    return AutoNumeric.unformat(val, this.currencyMaskOptions);
  }
}
