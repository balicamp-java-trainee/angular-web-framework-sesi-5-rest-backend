@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileMetadataDto{
   private Date uploadDate;
   private String fileNameWithoutExtension;
   private String fileFullName;
   private String fileExtention;
   private String fileLocation;
   private String fileCeksum;
   private BigDecimal fileSize;
   private String fileFullPath;
   private String fileType;
}
