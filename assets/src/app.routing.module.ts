import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductDetailComponent } from './product/product-detail/product-detail.component';
import { CreateProductComponent } from './product/create-product/create-product.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'product',
    pathMatch: 'full'
  },
  {
    path: 'product',
    component: ProductListComponent,
    data: { title : 'Data Product'}
  },
  {
    path: 'product-detail/:id',
    component: ProductDetailComponent,
    data: { title : 'Data Detail'}
  },
  {
    path: 'product-create',
    component: CreateProductComponent,
    data: { title : 'Add New Product'}
  },
  {
    path: 'product-edit/:id',
    component: CreateProductComponent,
    data: { title : 'Edit Product'}
  }

];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
