package co.id.balicamp.javatrainee.productcatalog.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-03-11
 * Time: 00:17
 */
@Data
@EqualsAndHashCode(callSuper= false)
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponseDto {
    private String status = "success";
    private String message;
}