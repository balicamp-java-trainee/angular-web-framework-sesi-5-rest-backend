import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpRequest, HttpHeaders, HttpClient } from '@angular/common/http';
import { errorHandler } from '@angular/platform-browser/src/browser';

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {

  private productApiUrl = "http://localhost:8189/transaction/upload-file";
  private httpOptions = {
    reportProgress: true,
    headers: new HttpHeaders({
      'enctype': 'multipart/form-data'
    })
  };

  constructor(private https: HttpClient) { }

  private errorHandler = (error) => {
    var errorMsg = "Internal Server Error";
    switch(error.status){
      case "404":
        errorMsg = "Page Not Found";
        break;
      default:
        break;
    }
    return Observable.throw(errorMsg);
  }

  pushFileToStorage(file: any): Observable<HttpEvent<{}>> {
    const req = new HttpRequest('POST', this.productApiUrl, file, this.httpOptions);
    return this.https.request(req).pipe(
      catchError(this.errorHandler)
    );
  }
}
