import { FormControl, FormGroup } from '@angular/forms';
import { OnInit } from '@angular/core';
import { AuthTokenService } from './auth-token.service';
import { Router } from '@angular/router';
export abstract class PageActionAccessor {

    /** session checker */
    sessionActive: boolean = true;
    constructor() { }

    setSessionActive(err?: any | null){
        if (err.includes('401')) {
            this.sessionActive = false;
        }
    }
    isSessionActive(): boolean {
        return this.sessionActive;
    }
}