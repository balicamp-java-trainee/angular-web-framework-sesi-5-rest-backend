import { Component, OnInit } from '@angular/core';
import { Product } from '../model/product.model';
import { ProductService } from '../service/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products: Product[];

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.fetchData();
  }

  fetchData(){
    this.productService.getProducts().subscribe(data => {
      this.products = data;
    },
    err => {
      console.error(err)}
    );
  }
}
