import { PageActionAccessor } from 'src/app/helper/page-action.accessor';
import { AuthTokenService } from './../helper/auth-token.service';
import { Component, OnInit, Output, EventEmitter, AfterViewChecked, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit{

  constructor(private authService: AuthTokenService, private route: Router) {
  }

  ngOnInit() {
  }
  
  logout() {
    this.authService.logout();
  }

  directToLogin() {
    this.logout();
    this.route.navigate(['/home']);
  }
}
