package co.id.balicamp.javatrainee.productcatalog.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-03-08
 * Time: 18:54
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto extends BaseResponseDto {
    private String id;
    private String code;
    private String name;
    private BigDecimal weight;
    private BigDecimal price;
}
