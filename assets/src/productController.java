@CrossOrigin(origins="http://localhost:4200", maxAge=3600)
@RestController
public class ProductController {
  @Autowired
  private IProductService productService;

  @RequestMapping("/")
  public String hello(){
      return "Hello Spring Boot";
  }

  @RequestMapping(value = "/view-products",
    method = RequestMethod.POST)
  public ResponseEntity<List<ProductDto>> getProducts(
    Pageable page)  {
      return new ResponseEntity<List<ProductDto>>
        (productService.getAllProducts(page), HttpStatus.OK);
  }

  @RequestMapping(value = "/view-products/{idProduct}",
    method = RequestMethod.POST)
  public ResponseEntity<ProductDto> getProductById(
    @PathVariable("idProduct") String id) {
      return new ResponseEntity<ProductDto>
        (productService.getProductById(id), HttpStatus.OK);
  }

  @RequestMapping(value = "/transaction-add-product",
    method = RequestMethod.POST)
  public ResponseEntity<String> addProduct(
    @RequestBody ProductDto product) {
      return new ResponseEntity<String>
        (productService.addDataProduct(product), HttpStatus.OK);
  }

  @RequestMapping(value = "/transaction-update-product",
    method = RequestMethod.POST)
  public ResponseEntity<String> updateProduct(
    @RequestBody ProductDto product){
      return new ResponseEntity<String>
        (productService.updateDataProduct(product),
          HttpStatus.OK);
  }

  @RequestMapping(value = "/transaction-delete-product/{id}",
    method = RequestMethod.POST)
  public ResponseEntity<String> deleteProduct(@PathVariable String id){
      return new ResponseEntity<String>
        (productService.deleteProductById(id), HttpStatus.OK);
  }
}
