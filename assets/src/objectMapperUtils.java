private static ModelMapper modelMapper = new ModelMapper();

static {
    modelMapper = new ModelMapper();
    modelMapper.getConfiguration()
      .setMatchingStrategy(MatchingStrategies.STRICT);
}

private ObjectMapperUtils(){ }

public static <D, T> D map(final T entity, Class<D> outClass) {
    return modelMapper.map(entity, outClass);
}

public static <D, T> List<D> mapAll(
  final Collection<T> entityList, Class<D> outCLass) {
    return entityList.stream()
            .map(entity -> map(entity, outCLass))
            .collect(Collectors.toList());
}

public static <S, D> D map(final S source, D destination) {
    modelMapper.map(source, destination);
    return destination;
}
