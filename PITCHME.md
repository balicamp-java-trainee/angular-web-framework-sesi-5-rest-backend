---?image=assets/img/bg_logo.png
@title[Angular Rest Backend]

@snap[east span-50 text-white]
## @color[white](Angular<br>Rest Backend)
@snapend

@snap[south docslink span-50]
[Angular Docs](https://angular.io/)
@snapend

---?image=assets/img/bg_logo.png
@title[Angular Rest Backend - Roadmap]

@snap[north-east text-white]
ROADMAP
@snapend

@snap[east span-65]
@ul[spaced text-white]
- HTTP Request
- Processing Response
- Uploading File
@ulend
@snapend

---?image=assets/img/bg_logo.png
@title[Http Request]

@snap[east span-50 text-white]
## @color[white](HTTP Request)
@snapend

---?code=assets/src/pom.xml&lang=xml
@title[Modifying pom.xml]

@[2-5](Add JPA denpedency)
@[10-14](Add MySQL connector for java dependency)
@[20-25](Add modelmapper dependency for mapping entity class into object class)

@snap[north-east template-note text-black]
HTTP Request - Convert your existing java project with JPA
@snapend

---?code=assets/src/application.properties&lang=properties
@title[Modifying application.properties]

@[1-4](Fill all datasource properties)
@[5-7](Fill some jpa properties)

@snap[north-east template-note text-black]
HTTP Request - Adding some properties /resources/application.properties
@snapend

---?code=assets/src/productEntity.java&lang=java
@title[Adding Entity Class]

@snap[north-east template-note text-black]
HTTP Request - Adding Entity Class under package co.id.balicamp.javatrainee.productcatalog.entity
@snapend

---?code=assets/src/productDao.java&lang=java
@title[Modifying Dao Class]

@[6-7](extends superclass JpaRepository<T, ID> for using all method feature on superclass)


@snap[north-east template-note text-black]
HTTP Request - Modifying DAO Class & Depricated ~~implementation~~ class 😀
@snapend

---?color=lavender
@title[Modify Service]

```java
/* Before */
List<Product> getAllProducts();

/* After */
List<ProductDto> getAllProducts(Pageable page);
```

@[1-2](Before)
@[4-5](After -> add parameter Pageable from org.springframework.data.domain.Pageable)

@snap[north-east template-note text-black]
HTTP Request - Modifying Interface of Service
@snapend

---?code=assets/src/productServiceImpl.java&lang=java
@title[Modifying Service Implementation]

@[1-5](Before)
@[7-8](After -> Create property LOGGER from org.apache.commons.logging.Log)
@[10-26](After -> Complete code for return list of ProductDto)


@snap[north-east template-note text-black]
HTTP Request - Modifying Class implementation of Service
@snapend

---?code=assets/src/objectMapperUtils.java&lang=java
@title[Adding Object Mapper Util Class]

@[1-9](Create property and instance static configuration also make default constructor)
@[11-25](Create static method for mapping object with parameter source & destination)

@snap[north-east template-note text-black]
HTTP Request - Add some util class ```ObjectMapperUtils``` under co.id.balicamp.javatrainee.productcatalog.util
@snapend

---?code=assets/src/productController.java&lang=java
@title[Modifying Controller]

@[1,2](The @CrossOrigin is used to allow Cross-Origin Resource Sharing/CORS so that our angular application running on different server can consume these APIs from a browser)
@[12-18](Rest Controller for get all data product)
@[20-26](Rest Controller for get data product by ID)
@[28-34](Rest Controller for add data product)
@[36-43](Rest Controller for modify data product)
@[45-50](Rest Controller for delete products by ID)

@snap[north-east template-note text-black]
HTTP Request - Modifying your Controller Class
@snapend

---?image=assets/img/bg_logo.png
@title[Processing Response]

@snap[east span-50 text-white]
## @color[white](Processing Response)
@snapend

---?image=assets/img/bg.png
@title[Make Sample Angular App]

@snap[east span-50]
## @color[white](Angular App Client)
@snapend

```typescript
ng new balicamp-catalog


cd balicamp-catalog


ng serve
```
@[1,2, zoom-25](Create a sample angular project with angular cli command)
@[4-5, zoom-25](Go to your project directory)
@[7-8, zoom-20](Run ng serve command, open your browser and hit the url - http://localhost:4200/ and the angular app is up.)

@snap[north-east template-note text-white]
Processing Response - Make a sample angular app
@snapend

---?code=assets/src/ng-structure.txt&lang=text
@title[Project Structure]

@[16,16](Modules - Modules break up the application into logical pieces of code. Each piece of code or module is designed to perform a single task.All the modules are loaded by main.ts.)
@[4-9](Component - Component is used to bring the modules together.)
@[30,30](tsconfig.json - The presence of tsconfig.json file in a directory indicates that the directory is the root of a typescript project.This file specifies the root files and typescript compiler motions required to compile typescript.)
@[28,28](package.json - It contains all the dependencies defined for the angular project.Once, we do nom install, these dependencies are automatically downloaded.)
@[15,15](karma.conf.js - Config file for karma unit tests.)
@[26,26](The file angular.json will have all the application configurations such as info about the root and out directory.)

@snap[north-east template-note text-black]
Processing Response - Angular Project Structure
@snapend

---?code=assets/src/create-angular-client.script&lang=typescript
@title[Create Service & Component]

@[1,1](Go to your angular project directory)
@[3,3](Make some directory folder spesific for product)
@[5-10](Go to directory product and create some component using angular cli)
@[12](make some directory folder spesific for service)
@[14-15](Go to directory service and create some service component using angular cli)
@[17-18](Go up directory and create some directory for model)
@[20,21](Go to directory model and create some file : product.model.ts)

@snap[north-east template-note text-black]
Processing Response - Create Service & Component
@snapend

---?code=assets/src/product.model.ts&lang=typescript
@title[Create Model]

@snap[north-east template-note text-black]
Processing Response - Content of Product Model
@snapend

---?code=assets/src/product.service.ts&lang=typescript
@title[Product Service]

@[2,11-14](Import HttpHeader from angular/common/http and make global variable productApiUrl & httpOption on service component)
@[2,16-16](On constructor give private HttpClient variable : add existing import from angular/common/http)
@[18-32](Add some handleError function from HttpErrorResponse : add exising import from angular/common/http)
@[39-45](Add function getProducts for get list of data product)
@[34-37,42](Add function extractData for normalized http response from backend)
@[47-52](Add function getProductById for get data product by id as parameter request)
@[54-59](Add function insertProduct for save data product by data as parameter/body request)
@[61-66](Add function updateProduct for update data product by data as parameter/body request)
@[68-73](Add function deleteProduct for delete data product by id as parameter request)

@snap[north-east template-note text-black]
Processing Response - Product Service Component
@snapend

---?color=lavender
@title[Add bootstrap to Angular App]

```typescript
npm install bootstrap


// angular.json
"styles": [
  "node_modules/bootstrap/dist/css/bootstrap.min.css",
  "src/styles.css"
]

// app.modul.ts
bootstrap: [AppComponent]
```

@[1,1](Go to your angular project directory and install bootstrap using npm)
@[4-8](Adding style from bootstrap.min.css module)
@[10,11](app.modul.ts automatically add line bootstrap)

@snap[south docslink span-50]
[Bootstrap Documentation](https://getbootstrap.com/docs/4.0/components)
@snapend

@snap[north-east template-note text-black]
Processing Response - Installing bootstrap
@snapend

---?code=assets/src/product-list.component.ts&lang=typescript
@title[Product List Typescript]

@[12](Create global variable with type Array of Product model)
@[14](On constructor inject ProductService as private variable)
@[16-18](On ngOnInit angular lifecyle function add some sub-function)
@[20-27](On fetchData function we call service getProducts and subscribe & fill the product with response)

@snap[north-east template-note text-black]
Processing Response - Product List Component
@snapend

---?code=assets/src/product-list-fetchdata.component.html&lang=html
@title[Product List Html]

@[14-20](Use the Angular ngFor directive in the template to display each item in the product list)

@snap[north-east template-note text-black]
Processing Response - Product List HTML
@snapend

---?color=lavender
@title[Routing]

@snap[north-east text-black]
Routing with Angular CLI under src/app
@snapend

```npm
ng generate module app-routing --flat --module=app --spec=false
```
---?code=assets/src/app.routing.module.ts&lang=typescript
@title[Routing]

@[7-17](Add some route)
@[7-12](Default route)
@[13-17](Routes tell the router which view to display when a user clicks a link or pastes a URL into the browser address bar)
@[36-39](Delete the @NgModule.declarations array and delete CommonModule references too)
@[36-39](Add an @NgModule.exports array with RouterModule in it)
@[36-39](Add RouterModule to the @NgModule.imports array and configure it with the routes in one step by calling RouterModule.forRoot())

@snap[north-east template-note text-black]
Processing Response - Routing
@snapend

---?color=lavender
@title[Router Link-1]

```html
<router-outlet></router-outlet>

ng serve
```

@[1](Add dashboard link to the shell at app.component.html)
@[3](Run ng serve command, open your browser and hit the url - http://localhost:4200/ and the angular app is up.)

@snap[north-east template-note text-black]
Processing Response - Router Link
@snapend

---?code=assets/src/product-list-detail.component.html&lang=html
@title[Router Link-2]

@[3-8](Add router-link for create product)
@[21-22](Add router-link for view detail product)

@snap[north-east template-note text-black]
Processing Response - Router Link View Detail & Create Product | _product-list.component.html_
@snapend

---?code=assets/src/app.routing.module.ts&lang=typescript
@title[Router Link]

@[18-22](Add route for product-detail and parsing id as _router snapshot params_)
@[23-27](Add route for product-create)
@[28-32](Add route for product-edit and parsing id as _router snapshot params_)

@snap[north-east template-note text-black]
Processing Response - Router Link View Detail & Create Product | _app.routing.module.ts_
@snapend

---?code=assets/src/product-detail.component.ts&lang=typescript
@title[Router Link-3]

@[12](Create global variable with type map)
@[14-17](On constructor inject ProductService, Router & ActivatedRoute as private variable)
@[19-21](On ngOnInit angular lifecyle function add some sub-function)
@[23-31](On fetchData function we call service getProductById with parameter from _route.snapshot.params['id']_ and subscribe & fill the product with response)
@[33-42](Add function deleteProduct with parameter id and subscribe deleteProduct service than navigate route to product list)

@snap[north-east template-note text-black]
Processing Response - Product Detail | _product-detail.component.ts_
@snapend

---?code=assets/src/product-detail.component.html&lang=html
@title[Router Link-4]

@[1-27](Bind the product into html)
@[1-27](Let's Try _ng serve_ and open your browser at existing url http://localhost:4200)

@snap[north-east template-note text-black]
Processing Response - Product Detail | _product-detail.component.html_
@snapend

---?code=assets/src/create-product.component.ts&lang=typescript
@title[Router Link-5]

@[13](- Create global variable with type ProductForm import from _'@angular/forms'_ <br>- import in _app.module.ts_ : FormsModule,ReactiveFormsModule from _'@angular/forms'_)
@[14-21](Create some global variable)
@[23-25](On constructor inject ProductService, Router & ActivatedRoute as private variable)
@[27-33](On ngOnInit angular lifecyle function add some sub-function and init value of id & editMode)
@[35-56](Fill function initForm)
@[58-74](Add function onSubmit as event binding)
@[76-83](Add function doVerifyResult)
@[85-87](Add function goToList)

@snap[north-east template-note text-black]
Processing Response - Product Detail | _create-product.component.ts_
@snapend

---?code=assets/src/create-product.component.html&lang=html
@title[Router Link-6]

@[3](- Bind the productForm into html as property binding<br>- Event binding onSubmit form)
@[36-39](Using *ngIf directive for display some alert when something wrong by status expresion)
@[41-42](Bind the validation form as property binding, set enable style button when form valid)

@snap[north-east template-note text-black]
Processing Response - Product Detail | _create-product.component.html_
@snapend

---?image=assets/img/bg_logo.png
@title[Uploading File]

@snap[east span-50 text-white]
## @color[white](Uploading File)
@snapend

---?code=assets/src/step-file-upload.script&lang=typescript
@title[Create Upload Component]

@[1](Go to your angular project directory and create come component)
@[3-5](Make directory service and create service using angular CLI)

@snap[north-east template-note text-black]
Processing Response - Create Upload Component
@snapend

---?code=assets/src/upload-file.service.ts&lang=typescript
@title[Upload File Service]

@[10-16](Create private global variable)
@[18](Inject HttpClient as private parameter on constructor)
@[20-23](Create function pushFileToStorage)

@snap[north-east template-note text-black]
Processing Response - Upload File Service
@snapend

---?code=assets/src/upload-file.component.html&lang=html
@title[Upload File HTML]

@[1-15]()
@[16-31]()
@[32-51]()

@snap[north-east template-note text-black]
Processing Response - Upload File Component | _upload-file.component.html_
@snapend

---?code=assets/src/upload-file.component.ts&lang=typescript
@title[Upload File Typescript]

@[12-20](Init some global variable)
@[22](Create decorator @ViewChild using template reference #fileInput)
@[24](Inject upload service as private parameter on constructor component)
@[29-38](Create function selectFile using event binding on click input file upload)
@[40-56](Create function doUpload using event binding on click button Upload)
@[58-79](Create function doPush)
@[81-90](Create function to reset all variable to default value)
@[92-94](Create funtion doReset using event binding on click button reset)

@snap[north-east template-note text-black]
Processing Response - Upload File Component | _upload-file.component.ts_
@snapend


---?code=assets/src/FileUploadController.java&lang=java
@title[Upload File Controller]

@[26-44](Create Controller class on backend)

@snap[north-east template-note text-black]
Processing Response - Upload File Backend | _FileUploadController.java_
@snapend

---?code=assets/src/CorsFilter.java&lang=java
@title[CorsFilter]

@[1-13]()
@[14-35]()
@[36-40]()

@snap[north-east template-note text-black]
Processing Response - CorsFilter | Remark annotation _@CrossOrigin_ on each controller class
@snapend


---?code=assets/src/FileMetadataDto.java&lang=java
@title[Upload File DTO]

@[1-14](Create some property for file upload metadata)

@snap[north-east template-note text-black]
Processing Response - Upload File Backend | _FileMetadataDto.java_
@snapend

---?code=assets/src/FileSizeUnit.java&lang=java
@title[Upload File Size Enum]

@[1-17](Create enum class for file size unit)

@snap[north-east template-note text-black]
Processing Response - Upload File Backend - Utils | _FileSizeUnit.java_
@snapend

---?code=assets/src/FileMetaUtils.java&lang=java
@title[Upload File Utils]

@[21-29](Create method writeToFile)
@[52-65](Create method fileChecksum)
@[67-77](Create method getFilePathString)
@[31-50](Create method to write file to disk)
@[79-82](Create method getFilenameWithoutExtension)
@[84-88](Create method getExtension)
@[90-94](Create method getContentTypeFromSource)
@[96-117](Create method getSize)

@snap[north-east template-note text-black]
Processing Response - Upload File Backend - Utils | _FileMetaUtils.java_
@snapend

---?code=assets/src/FileServiceImpl.java&lang=java
@title[Upload File Service]

@[18-19](- Create property with type String)
@[18-19](- Add annotation @Value mapped from _application.properties_ with key _fileMetadata.base.dir_)
@[21-22](Inject FileMetaUtils)
@[24-33](Create method doUpload)
@[34-51](Create method doUpload)

@snap[north-east template-note text-black]
Processing Response - Upload File Backend - Utils | _FileServiceImpl.java_
@snapend

---?image=assets/img/bg_logo.png
@title[THANKYOU]

@snap[east span-50 text-white]
## @color[white](THANK'S)
@snapend
