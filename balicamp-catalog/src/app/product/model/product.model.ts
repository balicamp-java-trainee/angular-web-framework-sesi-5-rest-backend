export class Product{
    id: string;
    code: string;
    name: string;
    weight: number;
    price: number;
}