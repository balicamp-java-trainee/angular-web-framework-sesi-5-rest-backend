package co.id.balicamp.javatrainee.productcatalog.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class CustomPrincipal implements Serializable {

    private static final long serialVersionUID = 1L;
    private String username;
    private String email;

}

