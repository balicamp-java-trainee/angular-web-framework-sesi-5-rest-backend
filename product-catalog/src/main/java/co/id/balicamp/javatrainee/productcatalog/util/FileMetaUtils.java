package co.id.balicamp.javatrainee.productcatalog.util;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import java.awt.datatransfer.MimeTypeParseException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.security.MessageDigest;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-03-14
 * Time: 10:47
 */
@Component
public class FileMetaUtils {

    public String writeToFile(String baseDirectory, byte[] fileContent) throws Exception {
        String checksum = fileChecksum("MD5", fileContent);
        String filePathString = getFilePathString(checksum, baseDirectory, false);

        writeFile(filePathString, fileContent);

        return checksum;
    }

    public void writeFile(String filePath, byte[] fileContent) throws IOException {
        RandomAccessFile stream = new RandomAccessFile(filePath, "rw");
        FileChannel channel = stream.getChannel();

        FileLock lock = null;
        try {
            lock = channel.tryLock();
            ByteBuffer buffer = ByteBuffer.allocate(fileContent.length);
            buffer.put(fileContent);
            buffer.flip();
            channel.write(buffer);
            lock.release();
        } catch (final OverlappingFileLockException e) {
            stream.close();
            channel.close();
        }
        stream.close();
        channel.close();
    }

    public String fileChecksum(String hashAlgorithm, byte[] bytes) throws Exception {
        MessageDigest messageDigest = MessageDigest.getInstance(hashAlgorithm);
        messageDigest.update(bytes, 0, bytes.length);

        byte[] mdBytes = messageDigest.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < mdBytes.length; i++) {
            sb.append(Integer.toString((mdBytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    public String getFilePathString(String filename, String fileLocation, boolean appendCurrentTimeMillis) {
        StringBuilder sb = new StringBuilder();
        sb.append(fileLocation);
        sb.append(File.separator);
        if (appendCurrentTimeMillis) {
            sb.append(System.currentTimeMillis());
        }
        sb.append(filename);
        return sb.toString();
    }

    public String getFilenameWithoutExtension(String fileName) {
        String fn = fileName.substring(0, fileName.lastIndexOf("."));
        return fn;
    }

    public String getExtension(String fileName) {
        String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
        return fileExtension;
    }

    /**
     * @param p_fileName
     *            fileName of file
     * @return String of Content-Type of file by fileName
     * @throws MimeTypeParseException
     */
    public String getContentTypeFromSource(String p_fileName) throws MimeTypeParseException {
        return p_fileName == null ? "application/octet-stream" : new MimetypesFileTypeMap().getContentType(p_fileName);
    }

    /**
     * @param file
     *            {@link MultipartFile} file to be get size
     * @param sizeUnit
     *            point to {@link FileSizeUnit}
     * @return
     */
    public BigDecimal getSize(MultipartFile file, FileSizeUnit sizeUnit) {
        long bytes = file.getSize();
        double kilobytes = (bytes / 1024);
        double megabytes = (kilobytes / 1024);
        double gigabytes = (megabytes / 1024);
        double terabytes = (gigabytes / 1024);
        double petabytes = (terabytes / 1024);
        double exabytes = (petabytes / 1024);
        double zettabytes = (exabytes / 1024);
        double yottabytes = (zettabytes / 1024);

        BigDecimal size = BigDecimal.ZERO;
        if (sizeUnit.equals(FileSizeUnit.bytes)) {
            size = BigDecimal.valueOf(bytes);
        } else if (sizeUnit.equals(FileSizeUnit.kilobytes)) {
            size = BigDecimal.valueOf(kilobytes);
        } else if (sizeUnit.equals(FileSizeUnit.megabytes)) {
            size = BigDecimal.valueOf(megabytes);
        } else if (sizeUnit.equals(FileSizeUnit.gigabytes)) {
            size = BigDecimal.valueOf(gigabytes);
        } else if (sizeUnit.equals(FileSizeUnit.terabytes)) {
            size = BigDecimal.valueOf(terabytes);
        } else if (sizeUnit.equals(FileSizeUnit.petabytes)) {
            size = BigDecimal.valueOf(petabytes);
        } else if (sizeUnit.equals(FileSizeUnit.exabytes)) {
            size = BigDecimal.valueOf(exabytes);
        } else if (sizeUnit.equals(FileSizeUnit.zettabytes)) {
            size = BigDecimal.valueOf(zettabytes);
        } else if (sizeUnit.equals(FileSizeUnit.yottabytes)) {
            size = BigDecimal.valueOf(yottabytes);
        }

        return size;
    }
}
