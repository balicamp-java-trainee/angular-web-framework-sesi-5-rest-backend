import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, NgForm, FormControl } from '@angular/forms';
import { ProductService } from '../service/product.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  productForm: FormGroup;
  editMode = false;
  id: string;
  status: boolean = true;

  code: string = '';
  name: string = '';
  weight: number = 0;
  price: number = 0;

  constructor(private productService: ProductService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.editMode = params['id'] != null;
    });
    this.initForm();
  }

  initForm() {
    if (this.editMode) {
      this.productService.getProductById(this.id)
      .subscribe(res => {
        if (res) {
          this.productForm.get('id').setValue(this.id);
          this.productForm.get('code').setValue(res.code);
          this.productForm.get('name').setValue(res.name);
          this.productForm.get('weight').setValue(res.weight);
          this.productForm.get('price').setValue(res.price);
        }
      });
    }

    this.productForm = new FormGroup({
      'id': new FormControl(this.id),
      'code': new FormControl(this.code, Validators.required),
      'name': new FormControl(this.name, Validators.required),
      'weight': new FormControl(this.weight),
      'price': new FormControl(this.price)
    });
  }

  onSubmit() {
    if (this.editMode) {
      this.productService.updateProduct(this.productForm.value)
        .subscribe(res => {
          this.doVerifyResult(res);
        }, err => {
            console.log(err)
        });
    } else {
      this.productService.insertProduct(this.productForm.value)
        .subscribe(res => {
          this.doVerifyResult(res);
        },err => {
            console.log(err)
        });
    }
  }

  doVerifyResult(res) {
    if (res.status == 'success') {
      this.status = true;
      this.goToList();
    } else {
      this.status = false;
    }
  }

  goToList() {
    this.router.navigate(['/product']);
  }
}
