package co.id.balicamp.javatrainee.productcatalog.service.impl;

import co.id.balicamp.javatrainee.productcatalog.entity.ProductEntity;
import co.id.balicamp.javatrainee.productcatalog.model.BaseResponseDto;
import co.id.balicamp.javatrainee.productcatalog.model.ProductDto;
import co.id.balicamp.javatrainee.productcatalog.repository.ProductDao;
import co.id.balicamp.javatrainee.productcatalog.service.IProductService;
import co.id.balicamp.javatrainee.productcatalog.util.ObjectMapperUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-03-08
 * Time: 18:57
 */
@Service
public class ProductServiceImpl implements IProductService {

    protected Log LOGGER = LogFactory.getLog(this.getClass());

    @Autowired
    private ProductDao productDao;

    @Override
    public List<ProductDto> getAllProducts(Pageable page){
        try {
            //Pageable sortByProductCode = new PageRequest(0, 1000, Sort.Direction.ASC, "code");
            Page<ProductEntity> productEntities = productDao.findAll(page);
            List<ProductDto> productDtos = new ArrayList<>();
            if(productEntities != null) {
                productDtos = ObjectMapperUtils.mapAll(productEntities.getContent(), ProductDto.class);
            }
            return productDtos;
        }catch (Exception e) {
            LOGGER.error("error while get all data products ", e);
            return null;
        }
    }

    @Override
    public ProductDto getProductById(String id){
        try {
            ProductEntity productEntity = productDao.getOne(id);
            ProductDto productDto = ObjectMapperUtils.map(productEntity, ProductDto.class);
            return productDto;
        }catch (Exception e){
            LOGGER.error("error while get data product ", e);
            return null;
        }
    }

    @Override
    public BaseResponseDto addDataProduct(ProductDto product){
        try {
            ProductEntity productEntity = ObjectMapperUtils.map(product, ProductEntity.class);
            productDao.save(productEntity);
            return new BaseResponseDto("success", String.format("Success save product : %s", product.toString()));
        }catch (Exception e){
            LOGGER.error("error while save data product ", e);
            return new BaseResponseDto("error", "error while delete products!");
        }
    }

    @Override
    public BaseResponseDto updateDataProduct(ProductDto product) {
        try {
            ProductEntity productEntity = productDao.getOne(product.getId());
            if(productEntity != null){
                productEntity.setCode(product.getCode());
                productEntity.setName(product.getName());
                productEntity.setWeight(product.getWeight());
                productEntity.setPrice(product.getPrice());
                productDao.saveAndFlush(productEntity);
            }
            return new BaseResponseDto("success", String.format("Success update product with ID : %s", product.toString()));
        }catch (Exception e){
            LOGGER.error("error while update data product ", e);
            return new BaseResponseDto("error", "error while delete products!");
        }
    }

    @Override
    public BaseResponseDto deleteProductById(String id) {
        try {
            productDao.deleteById(id);
            return new BaseResponseDto("success", String.format("Success delete product with ID %s", id));
        }catch (Exception e){
            LOGGER.error("error while delete products ", e);
            return new BaseResponseDto("error", "error while delete products!");
        }
    }
}
