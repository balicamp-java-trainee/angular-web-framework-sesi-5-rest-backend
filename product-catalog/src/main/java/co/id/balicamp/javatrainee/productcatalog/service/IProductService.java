package co.id.balicamp.javatrainee.productcatalog.service;

import co.id.balicamp.javatrainee.productcatalog.model.BaseResponseDto;
import co.id.balicamp.javatrainee.productcatalog.model.ProductDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-03-08
 * Time: 18:53
 */
public interface IProductService {
    List<ProductDto> getAllProducts(Pageable page);

    ProductDto getProductById(String id);

    BaseResponseDto addDataProduct(ProductDto product);

    BaseResponseDto updateDataProduct(ProductDto product);

    BaseResponseDto deleteProductById(String id);
}
