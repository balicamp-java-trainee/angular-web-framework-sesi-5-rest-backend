@Entity (name = "product")
@Data
public class ProductEntity {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    @NotNull @NotEmpty @Size(max = 100) private String code;
    @NotNull @NotEmpty private String name;
    @Min(1) private BigDecimal weight;
    @Min(1000) private BigDecimal price;
}
