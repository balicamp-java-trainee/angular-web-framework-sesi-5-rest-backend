package co.id.balicamp.javatrainee.productcatalog.controller;

import co.id.balicamp.javatrainee.productcatalog.model.FileMetadataDto;
import co.id.balicamp.javatrainee.productcatalog.service.IFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-03-14
 * Time: 10:21
 */
@RestController
public class FileUploadController {

    @Autowired
    private IFileService fileService;

    @RequestMapping(value = "/transaction/upload-file", method = RequestMethod.POST)
    public ResponseEntity<List<FileMetadataDto>> doUploadFiles(MultipartHttpServletRequest multipartRequest)  {
        if (multipartRequest.getFileMap().isEmpty()){
//            BaseResponseDto responseDto = new BaseResponseDto("error",
//                    "Uploaded file is empty or the chosen file has no content, please select a file!");
            Map<String, Object> resp = new HashMap<>();
            resp.put("status", "error");
            resp.put("message", "Uploaded file is empty or the chosen file has no content, please select a file!");
            return new ResponseEntity(resp, HttpStatus.OK);
        }else{
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
            headers.add(HttpHeaders.PRAGMA, "no-cache");
            headers.add(HttpHeaders.EXPIRES, "0");
            return new ResponseEntity<List<FileMetadataDto>>(fileService.doUpload(multipartRequest),
                    headers, HttpStatus.OK);
        }
    }
}
