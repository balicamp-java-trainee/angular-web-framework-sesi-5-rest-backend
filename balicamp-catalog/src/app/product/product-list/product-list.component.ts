import { AutoNumeric } from 'autonumeric';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Product } from '../model/product.model';
import { ProductService } from '../service/product.service';
import { PageActionAccessor } from 'src/app/helper/page-action.accessor';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { ModalProductComponent } from '../modal-product/modal-product.component';
import { AuthTokenService } from 'src/app/helper/auth-token.service';
import { SessionCheckerService } from 'src/app/helper/session-checker.service';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent extends PageActionAccessor implements OnInit {

  products: any;

  /** material */
  rows = this.products;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = ['code', 'name', 'weight', 'price'];

  constructor(
    private productService: ProductService,
    private dialog: MatDialog,
    private route: Router,
    private authService: AuthTokenService
  ) {
    super();
  }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.productService.getProducts().subscribe(data => {
      this.products = data;
      this.rows = new MatTableDataSource(this.products);
      this.rows.paginator = this.paginator;
      this.rows.sort = this.sort;
    }, err => {
      this.setSessionActive(err);
      console.error(err)
    });
  }

  formatNumber(value) {
    return Number(value).toLocaleString('id-IDR', { minimumFractionDigits: 2, maximumFractionDigits: 2 })
  }

  applyFilter(filterValue: string) {
    this.rows.filter = filterValue.trim().toLowerCase();
  }

  openDialog(event) {
    const dialogRef = this.dialog.open(ModalProductComponent, {
      data: event
    });

    dialogRef.afterClosed().subscribe(result => {
      this.fetchData();
    });
  }

  directToLogin() {
    this.authService.logout();
    this.route.navigate(['/home']);
  }
}