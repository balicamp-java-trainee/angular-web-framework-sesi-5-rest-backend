import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionCheckerService {

  private isSession: boolean = true;

  constructor() { }

  setIsSessionValid(flag: boolean){
    this.isSession = flag;
  }

  isSessionValid(){
    this.isSession;
  }
}
