package co.id.balicamp.javatrainee.productcatalog.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-03-14
 * Time: 10:28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileMetadataDto extends BaseResponseDto{
    private Date uploadDate;
    private String fileNameWithoutExtension;
    private String fileFullName;
    private String fileExtention;
    private String fileLocation;
    private String fileCeksum;
    private BigDecimal fileSize;
    private String fileFullPath;
    private String fileType;

    public FileMetadataDto(String status, String message) {
        super(status, message);
    }
}
